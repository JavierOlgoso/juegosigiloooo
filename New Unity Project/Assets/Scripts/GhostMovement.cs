using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform Destino;

    public List<Transform> destinationObjects;
    private int currentdestination;
    // Start is called before the first frame update
    void Start()
    {      
        agent.SetDestination(destinationObjects[0].position);
        currentdestination = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance<=0.1f)
        {
         
            
            currentdestination += 1;
            if (currentdestination >= destinationObjects.Count)
            {
                currentdestination = 0;
            }
            agent.SetDestination(destinationObjects[currentdestination].position);
        }
    }
}
